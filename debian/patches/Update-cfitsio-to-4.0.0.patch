From: Simon Conseil <s.conseil@ip2i.in2p3.fr>
Date: Thu, 29 Jul 2021 22:41:36 +0200
Subject: Update cfitsio to 4.0.0

https://github.com/astropy/astropy/pull/12006
---
 astropy/io/fits/hdu/compressed.py       | 46 +++++----------------------------
 astropy/io/fits/src/compressionmodule.c | 33 +----------------------
 astropy/io/fits/src/compressionmodule.h | 23 +++--------------
 3 files changed, 11 insertions(+), 91 deletions(-)

diff --git a/astropy/io/fits/hdu/compressed.py b/astropy/io/fits/hdu/compressed.py
index f571afd..5e1160f 100644
--- a/astropy/io/fits/hdu/compressed.py
+++ b/astropy/io/fits/hdu/compressed.py
@@ -57,21 +57,7 @@ DEFAULT_HCOMP_SMOOTH = 0
 DEFAULT_BLOCK_SIZE = 32
 DEFAULT_BYTE_PIX = 4
 
-CMTYPE_ALIASES = {}
-
-# CFITSIO version-specific features
-if COMPRESSION_SUPPORTED:
-    try:
-        CFITSIO_SUPPORTS_GZIPDATA = compression.CFITSIO_VERSION >= 3.28
-        CFITSIO_SUPPORTS_Q_FORMAT = compression.CFITSIO_VERSION >= 3.35
-        if compression.CFITSIO_VERSION >= 3.35:
-            CMTYPE_ALIASES['RICE_ONE'] = 'RICE_1'
-    except AttributeError:
-        # This generally shouldn't happen unless running pip in an
-        # environment where an old build of pyfits exists
-        CFITSIO_SUPPORTS_GZIPDATA = True
-        CFITSIO_SUPPORTS_Q_FORMAT = True
-
+CMTYPE_ALIASES = {'RICE_ONE': 'RICE_1'}
 
 COMPRESSION_KEYWORDS = {'ZIMAGE', 'ZCMPTYPE', 'ZBITPIX', 'ZNAXIS', 'ZMASKCMP',
                         'ZSIMPLE', 'ZTENSION', 'ZEXTEND'}
@@ -828,12 +814,6 @@ class CompImageHDU(BinTableHDU):
         # almost entirely contrived corner cases, so it will do for now
         if self._has_data:
             huge_hdu = self.data.nbytes > 2 ** 32
-
-            if huge_hdu and not CFITSIO_SUPPORTS_Q_FORMAT:
-                raise OSError(
-                    "Astropy cannot compress images greater than 4 GB in size "
-                    "({} is {} bytes) without CFITSIO >= 3.35".format(
-                        (self.name, self.ver), self.data.nbytes))
         else:
             huge_hdu = False
 
@@ -926,25 +906,11 @@ class CompImageHDU(BinTableHDU):
             # this behavior so the only way to determine which behavior will
             # be employed is via the CFITSIO version
 
-            if CFITSIO_SUPPORTS_GZIPDATA:
-                ttype2 = 'GZIP_COMPRESSED_DATA'
-                # The required format for the GZIP_COMPRESSED_DATA is actually
-                # missing from the standard docs, but CFITSIO suggests it
-                # should be 1PB, which is logical.
-                tform2 = '1QB' if huge_hdu else '1PB'
-            else:
-                # Q format is not supported for UNCOMPRESSED_DATA columns.
-                ttype2 = 'UNCOMPRESSED_DATA'
-                if zbitpix == 8:
-                    tform2 = '1QB' if huge_hdu else '1PB'
-                elif zbitpix == 16:
-                    tform2 = '1QI' if huge_hdu else '1PI'
-                elif zbitpix == 32:
-                    tform2 = '1QJ' if huge_hdu else '1PJ'
-                elif zbitpix == -32:
-                    tform2 = '1QE' if huge_hdu else '1PE'
-                else:
-                    tform2 = '1QD' if huge_hdu else '1PD'
+            ttype2 = 'GZIP_COMPRESSED_DATA'
+            # The required format for the GZIP_COMPRESSED_DATA is actually
+            # missing from the standard docs, but CFITSIO suggests it
+            # should be 1PB, which is logical.
+            tform2 = '1QB' if huge_hdu else '1PB'
 
             # Set up the second column for the table that will hold any
             # uncompressable data.
diff --git a/astropy/io/fits/src/compressionmodule.c b/astropy/io/fits/src/compressionmodule.c
index 67abdb0..5f07149 100644
--- a/astropy/io/fits/src/compressionmodule.c
+++ b/astropy/io/fits/src/compressionmodule.c
@@ -223,13 +223,11 @@ int compress_type_from_string(char* zcmptype) {
     } else if (0 == strcmp(zcmptype, "HCOMPRESS_1")) {
         return HCOMPRESS_1;
     }
-#ifdef CFITSIO_SUPPORTS_SUBTRACTIVE_DITHER_2
     /* CFITSIO adds a compression type alias for RICE_1 compression
        as a flag for using subtractive_dither_2 */
     else if (0 == strcmp(zcmptype, "RICE_ONE")) {
         return RICE_1;
     }
-#endif
     else {
         PyErr_Format(PyExc_ValueError, "Unrecognized compression type: %s",
                      zcmptype);
@@ -531,9 +529,7 @@ void configure_compression(fitsfile* fileptr, PyObject* header) {
     // BLANK in the header
     Fptr->cn_zblank = Fptr->cn_zzero = Fptr->cn_zscale = -1;
     Fptr->cn_uncompressed = 0;
-#ifdef CFITSIO_SUPPORTS_GZIPDATA
     Fptr->cn_gzip_data = 0;
-#endif
 
     // Check for a ZBLANK, ZZERO, ZSCALE, and
     // UNCOMPRESSED_DATA/GZIP_COMPRESSED_DATA columns in the compressed data
@@ -541,11 +537,9 @@ void configure_compression(fitsfile* fileptr, PyObject* header) {
     for (idx = 0; idx < tfields; idx++) {
         if (0 == strncmp(columns[idx].ttype, "UNCOMPRESSED_DATA", 18)) {
             Fptr->cn_uncompressed = idx + 1;
-#ifdef CFITSIO_SUPPORTS_GZIPDATA
         } else if (0 == strncmp(columns[idx].ttype,
                                 "GZIP_COMPRESSED_DATA", 21)) {
             Fptr->cn_gzip_data = idx + 1;
-#endif
         } else if (0 == strncmp(columns[idx].ttype, "ZSCALE", 7)) {
             Fptr->cn_zscale = idx + 1;
         } else if (0 == strncmp(columns[idx].ttype, "ZZERO", 6)) {
@@ -706,11 +700,9 @@ void configure_compression(fitsfile* fileptr, PyObject* header) {
         /* Ugh; the fact that cfitsio defines its version as a float makes
            preprocessor comparison impossible */
         fits_get_version(&version);
-        if ((version >= CFITSIO_LOSSLESS_COMP_SUPPORTED_VERS) &&
-                (0 == strcmp(tmp, "NONE"))) {
+        if (0 == strcmp(tmp, "NONE")) {
             Fptr->quantize_level = NO_QUANTIZE;
         } else if (0 == strcmp(tmp, "SUBTRACTIVE_DITHER_1")) {
-#ifdef CFITSIO_SUPPORTS_SUBTRACTIVE_DITHER_2
             // Added in CFITSIO 3.35, this also changed the name of the
             // quantize_dither struct member to quantize_method
             Fptr->quantize_method = SUBTRACTIVE_DITHER_1;
@@ -735,29 +727,6 @@ void configure_compression(fitsfile* fileptr, PyObject* header) {
             break;
         }
     }
-#else
-            Fptr->quantize_dither = SUBTRACTIVE_DITHER_1;
-        } else {
-            Fptr->quantize_dither = NO_DITHER;
-        }
-    } else {
-        Fptr->quantize_dither = NO_DITHER;
-    }
-
-    if (Fptr->quantize_dither != NO_DITHER) {
-        switch (get_header_int(header, "ZDITHER0", &(Fptr->dither_offset), 0, HDR_NOFLAG)) {
-          case GET_HEADER_FAILED:
-            return;
-          case GET_HEADER_DEFAULT_USED: // ZDITHER0 keyword no found
-            /* TODO: Find out if that's actually working and not invalid... */
-            Fptr->dither_offset = 0;
-            Fptr->request_dither_offset = 0;
-            break;
-          default:
-            break;
-        }
-    }
-#endif
 
     Fptr->compressimg = 1;
     Fptr->maxelem = imcomp_calc_max_elem(Fptr->compress_type,
diff --git a/astropy/io/fits/src/compressionmodule.h b/astropy/io/fits/src/compressionmodule.h
index fb37a50..7c13387 100644
--- a/astropy/io/fits/src/compressionmodule.h
+++ b/astropy/io/fits/src/compressionmodule.h
@@ -18,30 +18,15 @@
 #endif
 
 
-#if CFITSIO_MAJOR >= 3
-    #if CFITSIO_MINOR >= 35
-        #define CFITSIO_SUPPORTS_Q_FORMAT_COMPRESSION
-        #define CFITSIO_SUPPORTS_SUBTRACTIVE_DITHER_2
-    #else
-        /* This constant isn't defined in older versions and has a different */
-        /* value anyways. */
-        #define NO_DITHER 0
-    #endif
-    #if CFITSIO_MINOR >= 28
-        #define CFITSIO_SUPPORTS_GZIPDATA
+#if CFITSIO_MAJOR == 3 && CFITSIO_MINOR < 35
+    #ifdef _MSC_VER
+        #pragma warning ( "your CFITSIO version is too old; use 3.35 or later" )
     #else
-        #ifdef _MSC_VER
-            #pragma warning ( "GZIP_COMPRESSED_DATA columns not supported" )
-        #else
-            #warning "GZIP_COMPRESSED_DATA columns not supported"
-        #endif
+        #warning "your CFITSIO version is too old; use 3.35 or later"
     #endif
 #endif
 
 
-#define CFITSIO_LOSSLESS_COMP_SUPPORTED_VERS 3.22
-
-
 /* These defaults mirror the defaults in io.fits.hdu.compressed */
 #define DEFAULT_COMPRESSION_TYPE "RICE_1"
 #define DEFAULT_QUANTIZE_LEVEL 16.0
