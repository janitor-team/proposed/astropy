Source: astropy
Maintainer: Debian Astronomy Maintainers <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: python
Priority: optional
Build-Depends: cython3,
               debhelper-compat (= 13),
               dh-python,
               graphviz,
               libcfitsio-dev (>= 3.35~),
               libexpat1-dev,
               libjs-jquery,
               libjs-jquery-datatables,
               libjs-mathjax,
               pkg-config,
               python3-all-dev,
               python3-astropy-sphinx-theme,
               python3-configobj,
               python3-erfa (>= 1.7.2),
               python3-extension-helpers,
               python3-h5py,
               python3-jinja2,
               python3-matplotlib (>= 3.0),
               python3-numpy (>= 1:1.17),
               python3-ply (>= 3.5),
               python3-pytest (>= 3.1),
               python3-pytest-astropy (>= 0.8),
               python3-pytest-mpl,
               python3-scipy (>= 1.1),
               python3-setuptools,
               python3-setuptools-scm,
               python3-sphinx (>= 1.3),
               python3-sphinx-astropy (>= 1.3),
               python3-sphinx-automodapi,
               python3-yaml (>= 3.13),
               wcslib-dev (>= 5.8),
               zlib1g-dev
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/debian-astro-team/astropy
Vcs-Git: https://salsa.debian.org/debian-astro-team/astropy.git
Homepage: https://astropy.org
Rules-Requires-Root: no

Package: python3-astropy
Architecture: any
Depends: python3-configobj,
         python3-distutils,
         python3-pytest (>= 3.1),
         python3-pytest-astropy,
         ${misc:Depends},
         ${python3-ply:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Recommends: libjs-jquery,
            libjs-jquery-datatables,
            python3-scipy
Suggests: libxml2-utils,
          python-astropy-doc,
          python3-astropy-affiliated,
          python3-bs4,
          python3-h5py,
          python3-matplotlib,
          python3-pandas,
          python3-tz,
          python3-yaml
Breaks: python3-astroquery (<< 0.4.3~),
        python3-gammapy (<< 0.18~),
        python3-gwcs (<< 0.12~),
        python3-poliastro (<< 0.15~),
        python3-specutils (<< 1.3~),
        python3-sunpy (<< 3.0~)
Description: Core functionality for performing astrophysics with Python
 The astropy package contains core functionality and some common tools
 needed for performing astronomy and astrophysics research with Python.
 It can be extended by a number of "affiliated packages" that are
 intended to work with the core package.

Package: python-astropy-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: Core functionality for performing astrophysics with Python (doc)
 The astropy package contains core functionality and some common tools
 needed for performing astronomy and astrophysics research with Python.
 It can be extended by a number of "affiliated packages" that are
 intended to work with the core package.
 .
 This package contains the package documentation.

Package: astropy-utils
Architecture: all
Section: science
Depends: python3-astropy (<< ${source:Version}.1~),
         python3-astropy (>= ${source:Version}),
         python3-pkg-resources,
         ${misc:Depends},
         ${python3:Depends}
Breaks: pyfits-utils (<<1:3.3-4~)
Replaces: pyfits-utils (<<1:3.3-4~)
Description: Command line tools from astropy
 The astropy package contains core functionality and some common tools
 needed for performing astronomy and astrophysics research with Python.
 .
 This package contains the tools that come with astropy:
 .
  * fitscheck: Detect and fix FITS standards violations
  * fits2bitmap: Create a bitmap file from a FITS image.
  * fitsdiff: Compare two FITS image files and report the differences in
    header keywords and data.
  * fitsheader: Print the header(s) of one or more FITS file(s) to the
    standard output in a human-readable format.
  * samp_hub: SAMP Hub Server.
  * volint: Check a VOTable file for compliance to the VOTable specification
  * wcslint: Check the WCS keywords in a FITS file for compliance against
    the standards
